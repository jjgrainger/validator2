<?php

use Validation\Rules\Min;
use PHPUnit\Framework\TestCase;

class MinTest extends TestCase
{
    function setUp()
    {
        $this->min = new Min;
    }

    function testBetweenPassesWithValidInteger()
    {
        $passes = $this->min->run(2, [1]);

        $this->assertTrue($passes);
    }

    function testBetweenPassesWithValidString()
    {
        $passes = $this->min->run('abc', [1]);

        $this->assertTrue($passes);
    }

    function testBetweenFailsWithInteger()
    {
        $fails = $this->min->run(1, [2]);

        $this->assertFalse($fails);
    }

    function testBetweenFailsWithString()
    {
        $fails = $this->min->run('a', [2]);

        $this->assertFalse($fails);
    }
}
