<?php

use Validation\Rules\Required;
use PHPUnit\Framework\TestCase;

class RequiredTest extends TestCase
{
    function setUp()
    {
        $this->required = new Required;
    }

    function testRequiredPassesWithString()
    {
        $passes = $this->required->run('This will pass', []);

        $this->assertTrue($passes);
    }

    function testRequiredPassesWithInteger()
    {
        $passes = $this->required->run(1, []);

        $this->assertTrue($passes);
    }

    function testRequiredFailsWithNull()
    {
        $fails = $this->required->run(null, []);

        $this->assertFalse($fails);
    }

    function testRequiredFailsWithFalse()
    {
        $fails = $this->required->run(false, []);

        $this->assertFalse($fails);
    }

    function testRequiredFailsWithEmptyString()
    {
        $fails = $this->required->run('', []);

        $this->assertFalse($fails);
    }
}
