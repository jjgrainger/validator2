<?php

use Validation\Validator;
use PHPUnit\Framework\TestCase;

class ValidatorTest extends TestCase
{
    protected $v;

    public function setUp()
    {
        $this->v = new Validator;
    }

    public function testRulesetArrayCreatedFromString()
    {
        $rules = [
            'name' => 'required|between:3,5|alpha',
            'email' => 'required|email|min:3|unique:users,email'
        ];

        $expected = [
            'name' => [
                'required' => [],
                'between' => [3, 5],
                'alpha' => []
            ],
            'email' => [
                'required' => [],
                'email' => [],
                'min' => [3],
                'unique' => ['users', 'email']
            ]
        ];

        $rules = $this->v->createRuleset($rules);

        $this->assertEquals($expected, $rules);
    }

    public function testRulesetArrayCreatedFromArray()
    {
        $expected = [
            'name' => [
                'required' => [],
                'between' => [3, 5],
                'alpha' => []
            ],
            'email' => [
                'required' => [],
                'email' => [],
                'min' => [3],
                'unique' => ['users', 'email']
            ]
        ];

        $rules = $this->v->createRuleset($expected);

        $this->assertEquals($expected, $rules);
    }
}
