<?php

use Validation\Rules\Email;
use PHPUnit\Framework\TestCase;

class EmailTest extends TestCase
{
    function setUp()
    {
        $this->email = new Email;
    }

    function testEmailPassesWithValidEmail()
    {
        $passes = $this->email->run('name@mail.com', []);

        $this->assertTrue($passes);
    }

    function testEmailFailsWithInvalidEmail()
    {
        $fails = $this->email->run('thisisnotanemail', []);

        $this->assertFalse($fails);
    }
}
