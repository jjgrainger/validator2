<?php

use Validation\Rules\Between;
use PHPUnit\Framework\TestCase;

class BetweenTest extends TestCase
{
    function setUp()
    {
        $this->between = new Between;
    }

    function testBetweenPassesWithValidInteger()
    {
        $passes = $this->between->run(3, [1, 5]);

        $this->assertTrue($passes);
    }

    function testBetweenPassesWithValidString()
    {
        $passes = $this->between->run('abc', [1, 10]);

        $this->assertTrue($passes);
    }

    function testBetweenFailsWithInteger()
    {
        $fails = $this->between->run(6, [1, 5]);

        $this->assertFalse($fails);
    }

    function testBetweenFailsWithString()
    {
        $fails = $this->between->run('abcdef', [1, 5]);

        $this->assertFalse($fails);
    }
}
