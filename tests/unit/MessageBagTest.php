<?php

use Validation\Support\MessageBag;
use PHPUnit\Framework\TestCase;

class MinTest extends TestCase
{
    function testMessageBagInstantiatesWithEmptyArray()
    {
        $bag = new MessageBag;

        $this->assertEquals([], $bag->all());
    }
}
