<?php

namespace Validation\Support;

class MessageBag
{
    protected $items;

    public function __construct($messages = [])
    {
        $this->items = $messages;
    }

    public function set($key, $message)
    {
        $this->items[$key] = $message;
    }

    public function add($key, $message)
    {
        $this->items[$key][] = $message;
    }

    public function has($key)
    {
        return isset($this->items[$key]);
    }

    public function get($key, $default = null)
    {
        if ($this->has($key)) {
            return $this->items[$key];
        }

        return $default;
    }

    public function remove($key)
    {
        if ($this->has($key)) {
            unset($this->items[$key]);
        }
    }

    public function all()
    {
        return $this->items;
    }

    public function isEmpty()
    {
        return empty($this->items);
    }
}
