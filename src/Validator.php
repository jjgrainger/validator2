<?php

namespace Validation;

use Validation\Support\MessageBag;
use Validation\Contracts\ValidatorContract;

class Validator implements ValidatorContract
{

    /**
     * The data to validate
     * @var array
     */
    public $data;

    /**
     * The rules to use against data
     * @var array
     */
    public $rules;

    /**
     * Messages
     * @var array
     */
    public $messages;

    /**
     * Error bag
     * @var array
     */
    public $errors;

    /**
     * An array of instantiated rule objects
     * @var array
     */
    public $ruleObjects = [];

    /**
     * Create a validator object
     */
    public function __construct()
    {
        $this->messages = new MessageBag;
        $this->errors = new MessageBag;
    }

    /**
     * Validate data against a ruleset
     * @param  mixed  $data  Array or string to check
     * @param  array  $rules An array of rules
     * @return boolean       True if validation passes, false if fails
     */
    public function validate($data, $rules = [])
    {
        $this->data($data);

        // if rules have been passed
        if (!empty($rules)) {
            $this->rules($rules);
        }

        // run the validator
        // $this->run();
    }

    /**
     * Set the data to validate against
     * @param  array $data The data to validate
     * @return [type]       [description]
     */
    public function data($data)
    {
        if (!is_array($data)) {
            $data = ['input' => $data];
        }

        // add the data to check against
        $this->data = $data;

        return $this;
    }

    /**
     * Set the rules to validate data against
     * @param  array $rules An array of rules
     * @return [type]        [description]
     */
    public function rules($rules)
    {
        // convert ruleset to array
        // add them to the validator
        $this->rules = $this->createRuleset($rules);

        return $this;
    }

    /**
     * Return if tests pass
     * @return boolean
     */
    public function passes()
    {
        return $this->errors()->isEmpty();
    }

    /**
     * Return if the tests have failed
     * @return boolean
     */
    public function fails()
    {
        return ! $this->errors()->isEmpty();
    }

    /**
     * Return the errors object
     * @return MessageBag
     */
    public function errors()
    {
        return $this->errors;
    }

    /**
     * Return the messages objeect
     * @return MessageBag
     */
    public function messages()
    {
        return $this->messages;
    }

    /**
     * Add a rule to the Validator
     * @param string $rule  Name of the rule
     * @param Object $obj   Instantiated rule object
     */
    public function addRule($rule, $obj)
    {
        $this->ruleObjects[$rule] = $obj;
    }

    /**
     * Run the validator
     * @return [type] [description]
     */
    public function run()
    {
        // cycle through rules
        foreach ($this->rules as $field => $rules) {
            // if field exists in the data
            if (isset($this->data[$field])) {
                // grab the input
                $value = $this->data[$field];
                $input = $this->data;

                // cycle through rules
                foreach ($rules as $rule => $args) {
                    // if the validation fails
                    if (!$this->runRule($rule, $value, $input, $args)) {
                        // create an error
                        $this->createError($rule, $field, $value, $input, $args);
                    }
                }
            } else {
                // no field in data
            }
        }
    }

    /**
     * Resolve which rule class to run
     * @param  string $rule The name of rule
     * @return mixed        The rule object or false
     */
    public function resolveRule($rule)
    {
        // return false if no class found
        $ruleObject = false;

        // if a cached version of the object exists
        if (isset($this->ruleObjects[$rule])) {
            // return the cached object
            $ruleObject = $this->ruleObjects[$rule];
        } else {
            // otherwise find the class internally
            $class = "Validation\\Rules\\" . ucfirst($rule);

            // if the class exists
            if (class_exists($class)) {
                // set and return the cached object
                $ruleObject = $this->ruleObjects[$rule] = new $class;
            }
        }

        // return the rule object or false
        return $ruleObject;
    }

    /**
     * Run the rule validation against the input
     * @param  string $rule  The rule to run
     * @param  mixed  $input The data to validate
     * @param  array  $args  Additional arguments
     * @return boolean       True if passes, false if fails
     */
    public function runRule($rule, $value, $input, $args)
    {
        if ($ruleObject = $this->resolveRule($rule)) {
            // run the validation rule
            return call_user_func_array([$ruleObject, 'run'], [$value, $input, $args]);
        } else {
            // throw no rule exception
        }
    }

    /**
     * Create error message for failed rule
     * @param  string $rule  The rule name
     * @param  string $field The name of the field validated
     * @param  mixed  $input The field input
     * @param  array  $args  Additional arguments
     * @return string        The error message
     */
    public function createError($rule, $field, $value, $input, $args)
    {
        // check for custom error message
        $key = $field . '.' . $rule;

        // if there is a custom message
        if ($this->messages()->has($key)) {
            $format = $this->messages()->get($key);

        // use the rule default
        } else {
            $format = call_user_func([$this->ruleObjects[$rule], 'error']);
        }

        // create placeholders array
        $placeholders = [
            ':field' => str_replace('_', ' ', $field),
            ':value' => $value,
        ];

        // get custom placeholders for rule
        $custom = call_user_func_array([$this->ruleObjects[$rule], 'placeholders'], [$args]);

        // merge custom placeholders
        $placeholders = array_merge($placeholders, $custom);

        // create find and replace arrays
        $find = array_keys($placeholders);
        $replace = array_values($placeholders);

        // create the error message
        $message = ucfirst(str_replace($find, $replace, $format));

        // add the message to the errors
        $this->errors()->add($field, $message);
    }

    /**
     * Create ruleset array from strings
     * @param  array  $rules Array of rules
     * @return array         Array of riles
     */
    public function createRuleset($rules)
    {
        $rulesArray = [];

        // foreach field and its rules passed
        foreach ($rules as $field => $ruleString) {
            // add the field to the rule array
            $rulesArray[$field] = [];

            // if its a rule string
            if (is_array($ruleString)) {
                // the developer has already passed an array
                $rulesArray[$field] = $ruleString;

                continue;
            } else {
                // seperate rules into array
                // required|between:3,5 = [required, between:3,4]
                $items = explode('|', $ruleString);

                // foreach rule and its arguments
                foreach ($items as $item) {
                    // seperate rulename and parts
                    // between:3,4 = ['between', '3,4']
                    $parts = explode(':', $item);

                    // first part is the rule
                    $rule = $parts[0];
                    $args = [];

                    // if the rule has arguments
                    if (isset($parts[1])) {
                        // create an array of arguments
                        $args = explode(',', $parts[1]);
                    }

                    // add the rule and its arguments to
                    // the ruleset array
                    $rulesArray[$field][$rule] = $args;
                }
            }
        }

        // return the new ruleset array
        return $rulesArray;
    }

    // invoke validation inline
    public function __call($method, $args)
    {
        if ($this->resolveRule($method)) {
            // assign the method as a rule
            $this->rules['input'][$method] = $args;
        }

        return $this;
    }
}
