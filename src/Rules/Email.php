<?php

namespace Validation\Rules;

use Validation\Contracts\RuleContract;

class Email implements RuleContract
{
    public function run($value, $input, $args)
    {
        return filter_var($value, FILTER_VALIDATE_EMAIL) !== false;
    }

    public function placeholders($args)
    {
        return [];
    }

    public function error()
    {
        return ':field must be a valid email.';
    }
}
