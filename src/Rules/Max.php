<?php

namespace Validation\Rules;

use Validation\Contracts\RuleContract;

class Max implements RuleContract
{
    public function run($value, $input, $args)
    {
        list($max) = $args;

        if (is_string($value)) {
            $length = strlen($value);
        } elseif (is_int($value)) {
            $length = $value;
        }

        return ($length >= $max);
    }

    public function placeholders($args)
    {
        list($max) = $args;

        return [
            ':max' => $max
        ];
    }

    public function error()
    {

        return ':field must be less than :max.';
    }
}
