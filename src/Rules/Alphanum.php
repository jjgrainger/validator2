<?php

namespace Validation\Rules;

use Validation\Contracts\RuleContract;

class Alphanum implements RuleContract
{
    public function run($value, $input, $args)
    {
        return ctype_alnum($value);
    }

    public function placeholders($args)
    {
        return [];
    }

    public function error()
    {
        return ':field must be letters and numbers only.';
    }
}
