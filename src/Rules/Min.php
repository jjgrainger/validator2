<?php

namespace Validation\Rules;

use Validation\Contracts\RuleContract;

class Min implements RuleContract
{
    public function run($value, $input, $args)
    {
        list($min) = $args;

        if (is_string($value)) {
            $length = strlen($value);
        } elseif (is_int($value)) {
            $length = $value;
        }

        return ($length >= $min);
    }

    public function placeholders($args)
    {
        list($min) = $args;

        return [
            ':min' => $min
        ];
    }

    public function error()
    {
        return ':field must be less than :min.';
    }
}
