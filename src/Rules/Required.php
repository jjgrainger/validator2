<?php

namespace Validation\Rules;

use Validation\Contracts\RuleContract;

class Required implements RuleContract
{
    public function run($value, $input, $args)
    {
        return !empty($value);
    }

    public function placeholders($args)
    {
        return [];
    }

    public function error()
    {
        return ':field is required.';
    }
}
