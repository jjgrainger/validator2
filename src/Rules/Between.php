<?php

namespace Validation\Rules;

use Validation\Contracts\RuleContract;

class Between implements RuleContract
{
    public function run($value, $input, $args)
    {
        list($min, $max) = $args;

        if (is_string($value)) {
            $length = strlen($value);
        } elseif (is_int($value)) {
            $length = $value;
        }

        return ($length >= $min && $length <= $max);
    }

    public function placeholders($args)
    {
        list($min, $max) = $args;

        return [
            ':min' => $min,
            ':max' => $max
        ];
    }

    public function error()
    {
        return ':field must be between :min and :max.';
    }
}
