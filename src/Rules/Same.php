<?php

namespace Validation\Rules;

use Validation\Contracts\RuleContract;

class Same implements RuleContract
{
    public function run($value, $input, $args)
    {
        list($key) = $args;

        return $value === $input[$key];
    }

    public function placeholders($args)
    {
        list($key) = $args;

        return [
            ':other' => $key
        ];
    }

    public function error()
    {
        return ':field must be the same as :other.';
    }
}
