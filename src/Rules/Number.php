<?php

namespace Validation\Rules;

use Validation\Contracts\RuleContract;

class Number implements RuleContract
{
    public function run($value, $input, $args)
    {
        return is_numeric($value);
    }

    public function placeholders($args)
    {
        return [];
    }

    public function error()
    {
        return ':field must be numbers only.';
    }
}
