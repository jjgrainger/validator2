<?php

namespace Validation\Rules;

use Validation\Contracts\RuleContract;

class Alpha implements RuleContract
{
    public function run($value, $input, $args)
    {
        return ctype_alpha($value);
    }

    public function placeholders($args)
    {
        return [];
    }

    public function error()
    {
        return ':field must be letters only.';
    }
}
