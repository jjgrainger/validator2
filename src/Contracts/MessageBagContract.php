<?php

namespace Validator\Contracts;

interface MessaageBagContract
{
    public function add($key, $value);
    public function has($key);
    public function get($key, $default);
    public function first($key);
    public function all($key = null);
    public function remove($key);
}
