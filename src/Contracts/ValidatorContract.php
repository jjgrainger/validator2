<?php

namespace Validation\Contracts;

interface ValidatorContract
{
    public function validate($data, $rules = []);
    public function passes();
    public function fails();
    public function errors();
    public function messages();
}
