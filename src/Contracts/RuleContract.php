<?php

namespace Validation\Contracts;

interface RuleContract
{
    public function run($value, $input, $args);
    public function placeholders($args);
    public function error();
}
