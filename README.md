# Validation v1.0

> PHP Validation made easy

## Requirements

* PHP 5.4+

## Installation

Install with [Composer](#)

```
$ composer require jjgrainger/validation
```

## Basic usage

```php
use Validation\Validator;

$validator = new Validator;

$data = [
    'name' => 'Smith',
    'email' => 'smith@email.com'
];

$validator->validate($data, [
    'name' => 'required|min:3|alphanum',
    'email' => 'required|email'
]);

$validator->run(); // ?

if ($validator->passes()) {
    // do stuff
} else {
    // do something with errors
    $validator->errors()->all();
}
```
