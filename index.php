<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require __DIR__ . '/vendor/autoload.php';

use Validation\Validator;



class Unique implements Validation\Contracts\RuleContract
{
    // public $db;

    // public function __construct($db = null)
    // {
    //     $this->db = $db;
    // }

    public function run($value, $input, $args)
    {
        list($table, $column) = $args;

        return false;
    }

    public function placeholders($args)
    {
        list($table, $column) = $args;

        return [
            ':table' => $table,
            ':column' => $column
        ];
    }

    public function error()
    {
        return 'The :field :input already exists.';
    }
}



$v = new Validator;

$v->addRule('unique', new Unique);

$data = [
    'name' => 'alpha123#',
    'email' => 'joe@mail.com',
    'email_confirm' => 'joe@mail..com'
];

$v->messages()->set('email.unique', ':field already exists in :table.:column database');

$v->validate($data, [
    'name' => 'required|alphanum',
    'email' => 'required|email|unique:users,email',
    'email_confirm' => 'required|same:email'
]);

$v->run();

// $input = 'Joe';

// $v
//     ->required()
//     ->min(5)
//     ->between(5, 10)
//     ->validate($input);

// $v->run();

var_dump($v->errors()->all());

/*

    $v = new Validator;

    // validate inline with chainable methods
    $v->isEmail()->isRequired()->validate('joe@venncreative.co.uk'); // returns true/false

    // set error messages
    $v->messages([
        'name.required' => 'I need that shit man!',
        'email.required' => 'I need that shit man!'
    ]);

    // validate everything at once by passing array of data and rules
    // will return boolean
    $passes = $v->validate($data, [
        'name' => 'required|between:3,5',
        'email' => 'required|email|unique:user,email',
    ]);

    $v->validate($data, [
        'name' => [
            'required' => [],
            'min' => [3]
        ],
        'email' => [
            'required' => [],
            'email' => []
        ]
    ]);

    // check laters?
    $v->passes(); // true/false
    $v->fails();

    // get error messages
    $v->errors();

    // custom rules and overriding rules
    $v->attach('required', My\Validation\Rule::class);


 */
